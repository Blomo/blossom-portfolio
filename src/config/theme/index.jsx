export const theme = {
  primary: "#2D00FB",
  secondary: "#D7263D",
  lightBg: "#F4F5F6",
  primaryText: "#03002F",
  lightText: "#696969",
  normalText: "#424242",
  darkText: "#333333",
  
};