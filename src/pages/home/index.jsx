import React from 'react';
import { Bob } from '../../Component/Bob/styles';
import Footer from '../../Component/Footer2/index';
import Header from '../../Component/Header';
import Intro from '../../Component/Intro/index';
import Project from '../../Component/Project/index';
import Tools from '../../Component/Tools/index';
import More from '../../Component/More/index';


import {HomeContainer} from "./styles";

const Home=()=>{
return(
  <HomeContainer>
    <Header />
    <Bob
    width="270px"
    height="270px"/>
    <Intro/>
    <Tools/>
    <Project/>
    
  
    <More/>
    <Footer/>
  
    
  </HomeContainer>
)
};
export default Home;
