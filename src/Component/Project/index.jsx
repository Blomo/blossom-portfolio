import React from "react";
import Card from "../Card";
import { ProjectContainer } from "./styles";
import Logo from "../../assets/picture.png";
import Logo1 from "../../assets/logo1.png";
import  { Header} from "../Text/index"

const card = [
  {
    logo: Logo,
    text:
      "Embed is an educational platform built to enhance learning and make education fun and addictive. As a Front-end dev working on such a big project, I was happy build and implement reusable components, amongst other things. ",
    url: "#",
  },
  {
    logo: Logo1,
    text:
      "Embed is an educational platform built to enhance learning and make education fun and addictive. As a Front-end dev working on such a big project, I was happy build and implement reusable components, amongst other things. ",
    url: "https://inventhub.io/",
  },
];

const Project = () => {
  return (
    <ProjectContainer>
       <Header text="Projects"/>
      <div className="container">
        {card.map(({ logo, text, uri }) => (
          <Card logoImage={logo} about={text} />
        ))}
      </div>
    </ProjectContainer>
  );
};
export default Project;
