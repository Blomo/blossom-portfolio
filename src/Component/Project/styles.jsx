import styled from "styled-components";


export const ProjectContainer = styled.div`
  /* height: 65vh; */
  width: 100%;
  margin-top:20px;
  margin-bottom:20px;
  display: flex;
  flex-direction:column;
  justify-content: center;
  align-items: center;
  /* color: purple; */

  .container{
    width:100%;
    display: flex;
    flex-wrap:wrap;
    justify-content: space-around;
  align-items: center;
   
  }

  @media only screen and (max-width: 420px) {
    .container{
    flex-direction:column;
    justify-content:center;
    }
  }
`