import styled from "styled-components";
// import {theme} from "../../config/theme"

export const HeaderContainer = styled.div`
  background-color: ${({ headerHasNotMoved }) =>
    headerHasNotMoved ? "transparent" : "#b0afaa"};
  position: fixed;
  top: 0;
  overflow: none;
  z-index: 1;
  width: 100%;
  transition: background-color 0.6s ease-out;
  margin: 0;
  padding: 0;
  display: flex;
  height: max-content;
  justify-content: space-between;
  box-shadow: 0 4px 6px rgba(50, 50, 93, 0.1), 0 1px 3px rgba(0, 0, 0, 0.08);

  .logoContainer {
    height: 80px;
    width: 26%;
    display: flex;
    padding: 0 10px;
    justify-content: center;
    align-items: center;

    b {
      font-size: 2rem;
      font-family: "Quicksand", sans-serif;
    }
  }

  @media only screen and (max-width: 400px) {
    justify-content: flex-end;
    height: 60px;
    box-shadow: 0 0px 0px;
    background-color: ${({ headerHasNotMoved }) =>
      headerHasNotMoved ? "transparent" : "#ffffff"};

    .logoContainer {
      display: none;
    }

    .iconContainer {
      display: flex;
    }
  }
`;
