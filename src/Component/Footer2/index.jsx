import React from "react";
import {FooterContainer} from "./styles"
;

import { FaMediumM} from 'react-icons/fa';

import { IoLogoGoogle } from 'react-icons/io';
import {
  FiYoutube as Youtube,
  FiTwitter as Twitter,
  FiLinkedin as LinkedIn,
  FiInstagram as Instagram,
  FiGithub as Github
} from "react-icons/fi";


const Footer=()=>{

  const socialData = [
    { name: "twitter", icon: <Twitter />, url: "#" },
    { name: "linkedIn", icon: <LinkedIn />, url: "#" },
    { name: "instagram", icon: <Instagram />, url: "#" },
    { name: "github", icon: <Github />, url: "#" },
    { name: "youtube", icon: <Youtube />, url: "#" },
    {
      name:"linkIn", icon:<IoLogoGoogle/> ,url:""
    },
    {
      name:"medium",icon:<FaMediumM />
    }
  ];
  return(
    <FooterContainer>

    <div className="container">
      {/* <div className="socialMedia"> */}
        {/* <div></div> */}
        <div className="socialMedia">
        {socialData.map(({ name, icon, url }) => (
          <div className="socialcontainer" name={name}>{icon}</div>
        ))}
      </div>


      {/* </div> */}

      <div className="copyRight">
        <small>
        Copyright© Blossom Omamomo Onoriode 2020
        </small>
      </div>

    </div>
    </FooterContainer>
  )
}

export default  Footer;