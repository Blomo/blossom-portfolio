import styled from "styled-components";
// import { theme } from "../../config/theme";

export const FooterContainer = styled.div`
  background-color: #0b0e0b;
  height: 70px;
  display: flex;
  margin-top:20px;
  /* height: max-content; */
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem 1rem;
  margin: 0;
  color: white;

  .container {
    /* background-color: #a01299; */
    justify-content: space-between;
    width: 100%;
    display: flex;
   
    align-items: center;
    .socialMedia {
      display: flex;
      width: 40%;
      justify-content: space-between;
      
     
    cursor: pointer;
    display: flex;
    justify-content: space-around;
    align-items: center; 
    /* width:30%; */

    .socialcontainer {
      border-radius: 50%;
      -webkit-box-shadow: 0px 0px 7px 0px rgba(255, 255, 255, 1);
      -moz-box-shadow: 0px 0px 7px 0px rgba(255, 255, 255, 1);
      box-shadow: 0px 0px 7px 0px rgba(255, 255, 255, 1);
      width: max-content;
      height: max-content;
      padding: 0.5rem;
      display: flex;
      justify-content: space-around;
      align-items: center;
    }


      
    }
    /* .copyRight{
      /* color:#ffffff; */
      /* font-weight:bold; */
      /* font-family: "Quicksand", sans-serif; */


      
    /* } */ */
    .copyright {
   
   width:30%;
   small {
     font-weight: bolder; 
     cursor: pointer; 
   }
 }
  }
  @media only screen and (max-width: 515px) {
    .container {
   
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    .socialMedia {
     
      width:100%;
    }
    }

  }

`;
