import styled from "styled-components";


export const TriangleContainer = styled.div`
  width: 0pc;
  position:absolute;
  height: 0pc;
  /* background-color:blue; */
  top:${props=>props.top ?props.top : "50%"};
  left:${props=>props.left ?props.left : "75%"};
  border-left: ${props=>props.borderleft ?props.borderleft : "60px solid transparent"};
  border-right: ${props=>props.borderRight ?props.borderRight : "70px solid transparent"};
  border-bottom: ${props=>props.borderBottom ?props.borderBottom : "180px solid purple"};
  

  @media only screen and (max-width: 600px) {
  }
`;
