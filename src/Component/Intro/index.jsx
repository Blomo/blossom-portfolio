import React, { useState, useEffect } from "react";
import { IntroContainer } from "./styles";
import CustomButton from "../Button";
import { Text, Header } from "../Text/index";

const Intro = (...otherProps) => {
  const [introNotMoved, setIntroHasNotMoved] = useState(true);
  const [positionOfIntro, setPositionOfIntro] = useState(0);

  const scrollFunction = () => {
    const position = window.pageYOffset;
    setPositionOfIntro(position);
    position > 200 ? setIntroHasNotMoved(false) : setIntroHasNotMoved(true);
    console.log(positionOfIntro);
  };

  const Scroll = scrollFunction;

  useEffect(() => {
    window.addEventListener("scroll", Scroll, { passive: true });
    return () => {
      window.removeEventListener("scroll", Scroll);
    };
  }, [Scroll]);

  return (
    <IntroContainer introNotMoved={introNotMoved}>
      <div className="container">

      
         <div className="introcontainer">
          <div className="contain">
            <div className="about-me">
              <Header text=" I'm Onoriode Blossom" />

              <Text
                text={
                  "I'm a passionate front-end software engineer based in Nigeria, having an experrience of designing and building  Web and Mobile application with Javascript React,React-Native,Next.js and some others cool libraries and frameworks"
                }
                
              />
               <div className="buttonContainer">
            <CustomButton title={"More"} />
            <CustomButton title={"Projects"} />
          </div>
            </div>
           
          </div>
          <div className="Image"></div>

         
        </div> 
      </div>
    </IntroContainer>
  );
};
export default Intro;
