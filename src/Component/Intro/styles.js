import styled from "styled-components";
// import { theme } from "../../config/theme";

import Image from "../../assets/5.png"
export const IntroContainer = styled.div`
  height: 70vh;
  width: 100%;
 
  display: flex;
  justify-content: center;
  align-items:center;
  
  .container {
    

    .introcontainer {
      width: 100%;
      justify-content: center;
      display: flex;

      /* background-color:orange; */
      /* flex-direction: column; */
     
      

     

      .contain {
        display: flex;
        align-items:center;
        /* background-color:yellow; */
        justify-content:center;
        flex-direction: row;
        /* height:50vh; */
        .about-me{
          /* background-color:green; */
          width:95%;
        .buttonContainer{
        
        width:100%;
        display:flex;
        justify-content:space-evenly;
      }

        }
      }
      .Image{
          background-image:url(${Image});
          width:30%;
          height:45vh;
          /* background-repeat:no-repeat; */
          background-size:100%;
          /* background-color:yellow */
         
        }
      
    }
  }

  @media only screen and (max-width: 800px) {
    /* height: 100vh; */
  width: 100%;
  /* background-color:yellow; */
  display: flex;
  justify-content: center;
  
  align-items:center;


  .container {
    

    .introcontainer {
      flex-direction:column-reverse;
      height:80vh;
    

     

      .contain {
        display: flex;
       background-color:transparent;
        justify-content:center;
        flex-direction:column ;
        height:50vh;
        .about-me{

          
          width:100%;
          height:35vh;
          /* background-color:blue; */



          .buttonContainer{
        
        width:100%;
        display:flex;
        justify-content:space-evenly;
      }

        }

       
      
      }
      .Image{
         
         width:100%;
         height:40vh;
         background-size:100%
         
          

        }
      
    }
  }
  }
  @media only screen and (max-width: 1000px) {
    height: 80vh;
  width: 100%;
  /* background-color:yellow; */
  display: flex;
  justify-content: center;
  
  align-items:center;


  .container {
    background-color:transparent;

    .introcontainer {
      flex-direction:column-reverse;
      align-items:center;
      justify-items:center;
    

     

      .contain {
        display: flex;
       
        justify-content:center;
        flex-direction:column ;
        .about-me{

          
          width:100%;
        }

       
      
      }
      .Image{
         
         width:100%;
         height:80vh;
         align-self:center;
         background-size:100%;
 /* resize:contain; */
         background:cover;

         background-repeat:no-repeat;
         
          

        }
      
    }
  }
  }

  
`;
