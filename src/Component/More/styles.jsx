import styled from "styled-components";
// import { theme } from "../../config/theme";

export const MoreContainer = styled.div`
  
  height: 50vh;
  display: flex;
  justify-content: center;
  align-items: center;
  .container {
    width: 80%;
    flex-direction: column;
    display: flex;
    title {
      font-size: 40px;
    }
  }
  @media only screen and (max-width: 420px) {
    height: 30vh;
  }
`;