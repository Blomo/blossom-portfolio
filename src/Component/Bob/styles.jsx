import styled from "styled-components";

export const Bob = styled.div`


background-color:${props=>props.backgroundcolor ? props.backgroundcolor: "purple"};
border-radius:${props=>props.borderradius ? props.borderradius:"300px"};
height:${props=> props.height?props.height:"400px"};
width:${props=> props.width?props.width:"400px"};
position:${props=> props.position?props.position:"absolute"};
margin-top:${props=> props.margintop?props.margintop:"-5%"};
/* margin-left:-7%; */
margin-left:${props=> props.marginleft?props.marginleft:"-7%"}; 

@media only screen and (max-width: 400px) {

height:${props=> props.height?props.height:"100px"};
width:${props=> props.width?props.width:"100px"};
margin-top:${props=> props.margintop?props.margintop:"-40%"};
margin-left:${props=> props.marginleft?props.marginleft:"-40%"}; 

}


@media only screen and (max-width: 857px) {

border-radius:${props=>props.borderradius ? props.borderradius:"300px"};
height:${props=> props.height?props.height:"100px"};
width:${props=> props.width?props.width:"100px"};
position:${props=> props.position?props.position:"absolute"};
top:${props=> props.top?props.top:"0px"};
left:${props=> props.left?props.left:"-40px"};

}
@media only screen and (max-width: 1200px) {
  /* background-color:blue; */
  top:${props=> props.top?props.top:"-10px"};
left:${props=> props.left?props.left:"-10px"};
height:${props=> props.height?props.height:"100px"};
width:${props=> props.width?props.width:"100px"};

}

`