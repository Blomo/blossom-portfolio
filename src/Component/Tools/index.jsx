import React from "react";
import { ToolsContainer } from "./styles";
import { AiFillGithub, AiFillGitlab, AiOutlineHtml5} from "react-icons/ai"
import {SiApollographql, SiJavascript} from "react-icons/si"
import {FaReact, FaWordpressSimple} from "react-icons/fa"
import {DiCss3Full} from "react-icons/di"



const Card=[
  {
    
    icon:<SiJavascript  className="icon"/>,
    label : "JavaScript"
  },
  {
    
    icon:<FaReact className="icon"/>,
    label : "React"
  },
  {
    
    icon:<FaReact className="icon"/>,
    label : "React-Native"
  },
  {
    
    icon:<AiFillGithub  className="icon"/>,
    label : "Github"
  },
  {
    
    icon:<AiFillGitlab className="icon"/>,
    label : "Gitlab"
  },
  {
    
    icon:<SiApollographql  className="icon"/>,
    label : "Apollo"
  },
  {
    
    icon:<AiOutlineHtml5  className="icon"/>,
    label : "HTML5"
  },
  {
    
    icon:<DiCss3Full  className="icon" />,
    label : "CSS3"
  },
  {
    
    icon:<FaWordpressSimple  className="icon" />,
    label : "CSS3"
  }
  
]


const Tools = () => {


  return (
    <ToolsContainer >

        <h2>TOOLS</h2>
      <div className="container">

      {Card.map((({icon, label})=>(
         <div className="card">
           <div>{icon}</div>
      <div>{label}</div>

         </div>
         
        )
          
         

        ))}
       


       
    
        
      </div>
     
    </ToolsContainer>
  );
};
export default Tools;
