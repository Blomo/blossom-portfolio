import React from 'react';
import {Container} from "./styles";


const Card=({logoImage,about,...otherProps})=>{

return(
  <Container>
    <div className="container">
    <div className="logoContainer">
    <img src={logoImage} alt="kfkf" className="logo"/>
    {/* <p>EmBED</p> */}
    </div>
    
    <div className="about">
      {about}
    </div>

    </div>
    
  </Container>
)
};
export default Card;
