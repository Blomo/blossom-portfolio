import styled from "styled-components";
// import { theme } from "../../config/theme";

export const Container = styled.div`

width: 280px;
    height: 340px;


    width: 280px;
    height: 340px;
    margin-bottom: 20px;
    align-items: center;
    display: flex;

    justify-content: center;
    flex-direction: column;
    border-radius: 10px;
    box-shadow: 0 1px 6px rgba(50, 50, 93, 0.1), 0 1px 3px rgba(0, 0, 0, 0.08);

    background-color: white; 

    .logoContainer {
      margin: 20px;
      align-items: center;
      display: flex;
      height: 110px;
      flex-direction: column;
      justify-content: center;

      .logo {
        height: 80px;
        width: 80px;
        box-shadow: 0 1px 6px rgba(50, 50, 93, 0.1),
          0 1px 3px rgba(0, 0, 0, 0.08);

       
        border-radius: 40%;
      }
      .logo:hover {
        height: 70px;
        width: 70px;
        box-shadow: 0 0px 0px rgba(50, 50, 93, 0.1),
          0 0px 0px rgba(0, 0, 0, 0.08);
        transition: 0.4s ease-in;
        background-color: transparent;
        border-radius: 10%;
      }
      p {
        font-size: 18px;
      }
    }

    .about {
      font-size: 14px;
      width: 85%;
      text-align: center;
      /* color: #8a7d6f; */
      font-family: "Quicksand", sans-serif;
    }
  }
  @media only screen and (max-width: 420px) {
    .logoContainer {
      align-items: center;
      display: flex; 

      .logo {
        
        box-shadow: 0 1px 6px rgba(50, 50, 93, 0.1),
          0 1px 3px rgba(0, 0, 0, 0.08);
        
        border-radius: 50%;
      }
    }
  }

  @media only screen and (max-width: 800px) {
 
    .logoContainer {
      align-items: center;
      display: flex;
      justify-content: center;

      .logo {
        height: 80px;
        /* width: 75px; */
        box-shadow: 0 1px 6px rgba(50, 50, 93, 0.1),
          0 1px 3px rgba(0, 0, 0, 0.08);

        background-color: white;
        border-radius: 50%;
      }
    }
  /* } */
  
`;
