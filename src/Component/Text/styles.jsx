import styled from "styled-components";
// import { theme } from "../../config/theme";

export const TextContainer = styled.p`
  font-size: 1.2rem;
  text-align: center;
  /* color:blue; */
  flex-grow:1;
  font-family: "Quicksand", sans-serif;

  @media only screen and (max-width: 420px) {
    font-size: 16px;
    width: 100%;
    text-align: center;
    /* color: #8a7d6f; */
    font-family: "Quicksand", sans-serif;
  }
`;

export const HeaderContainer = styled.h1`
  font-size: 2rem;
  text-align: center;
  font-family: "Quicksand", sans-serif;

  @media only screen and (max-width: 420px) {
    font-size: 24px;
    width: 100%;
    text-align: center;
    /* color: #8a7d6f; */

    /* color: ${(props) => (props.color ? props.color : "purple")}; */
    font-family: "Quicksand", sans-serif;
  }
`;
